local sensorInfo = {
	name = "GetUnitsByName",
	desc = "Return id's of units of given type in whole team",
	author = "Lakomyon",
	date = "2018-05-26",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetTeamUnitsByDefs = Spring.GetTeamUnitsByDefs
local myTeamID = Spring.GetMyTeamID()

-- @description return energy resource info
-- @argument unitName [string] unique unit name
return function(unitName)
	local unitDefID = UnitDefNames[unitName].id
  Spring.Echo("Parameter is: " .. unitName)
  return SpringGetTeamUnitsByDefs(myTeamID, unitDefID)
end