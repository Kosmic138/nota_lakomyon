local sensorInfo = {
	name = "Wind",
	desc = "Return data of actual wind.",
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching 
 
step = 64 
minHeight, maxHeight = Spring.GetGroundExtremes()

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function checkHill(i, j) 
  return function()
    if(maxHeight - Spring.GetGroundHeight(i, j) < 50) then
      return true
    else
      return false
    end
  end
end

-- @description return hills positions
return function()
  a = {}
  count = 0
  maxX = Game.mapSizeX
  maxZ = Game.mapSizeZ
  
  
  --when hill found, skip a distance
  for i=0,maxX,2*step do
    for j=0,maxZ,2*step do
      hill = checkHill(i, j)
      if(maxHeight - Spring.GetGroundHeight(i, j) < 1) then
        a[count] = i
        count = count+1
        a[count] = j
        count = count+1
      end
    end
  end
  a["maxHeight"] = maxHeight
  a["height"] = Spring.GetGroundHeight(0, 0)
  a["maxX"] = maxX
  a["maxZ"] = maxZ
  return a
end