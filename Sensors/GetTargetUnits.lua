function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load target unit with selected loader.",
		parameterDefs = {
      
		}
	}
end   

local function ClearState(self)
  
end

-- speed-ups                                      
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	local loader = parameter.loader -- Number
	local target = parameter.target -- Number
  
  -- move to the target location
  local cmdID = CMD.MOVE
  
	local pointX, pointY, pointZ = SpringGetUnitPosition(target)
  targetPosition = Vec3(pointX, pointY, pointZ)
  
  SpringGiveOrderToUnit(loader, cmdID, targetPosition:AsSpringVector(), {})
  
  return RUNNING
end                  

function Reset(self)
	ClearState(self)
end
