local sensorInfo = {
	name = "Wind",
	desc = "Return data of actual wind.",
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching 
 
step = 30
parts = 10 
minHeight, maxHeight = Spring.GetGroundExtremes()

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return hills positions
return function()
  hillCoords = {}
  count = 1
  maxX = Game.mapSizeX
  maxZ = Game.mapSizeZ
  
  for i=0,maxX,step do
    for j=0,maxZ,step do
      if(maxHeight - Spring.GetGroundHeight(i, j) < 1) then
      
        hillAssigned = false
        for k=1,table.getn(hillCoords),1 do
          x = hillCoords[k]["x"]
          z = hillCoords[k]["z"]
          
          xDiff = i-hillCoords[k]["x"]
          zDiff = j-hillCoords[k]["z"]
          
          allHills = true
          for l=0,parts,1 do
            --compare maxHeight to height of the [new point X coord - (l'th step of comparison * (difference of the compared points in X / number of steps), new point Z coord - (l'th step of comparison * (difference of the compared points in Z / number of steps)]
            --basicaly compares n points on line between new point and the one stored in hillCoords already, if all the compared points between are hills, the found point is part of the hill already stored, else go to another
            
            if(maxHeight - Spring.GetGroundHeight(i-(l*(xDiff/parts)), j-(l*(zDiff/parts))) > 1) then
              allHills = false
            end
          end
          
          if(allHills) then
            hillAssigned = true
            
            --add hill to the original hill
            newX = (hillCoords[k]["count"]*hillCoords[k]["x"] + i) / (hillCoords[k]["count"]+1)
            newZ = (hillCoords[k]["count"]*hillCoords[k]["z"] + j) / (hillCoords[k]["count"]+1)
            
            hillCoords[k]["count"] = hillCoords[k]["count"] + 1
            hillCoords[k]["x"] = newX
            hillCoords[k]["z"] = newZ
          end
          
          
        end
        
        if(not hillAssigned) then
          --new hill found, save it
          hillCoords[count] = {}
          hillCoords[count]["x"] = i
          hillCoords[count]["z"] = j
          hillCoords[count]["count"] = 1
          count = count+1
        end
        
      end
    end
  end
  
  hillCoords["maxHeight"] = maxHeight
  return hillCoords
end