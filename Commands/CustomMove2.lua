function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load target unit with selected loader.",
		parameterDefs = {
			{ 
				name = "loader",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "target",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end   

local function ClearState(self)
  
end

local closeBenchmark = 80

local function IsClose(x1, z1, x2, z2)
  local result = true
  if(x1 > x2) then
    if(x1 - x2 > closeBenchmark) then
      result = false
    end
  else
    if(x2 - x1 > closeBenchmark) then
      result = false
    end
  end
  
  if(z1 > z2) then
    if(z1 - z2 > closeBenchmark) then
      result = false
    end
  else
    if(z2 - z1 > closeBenchmark) then
      result = false
    end
  end
  return result
end

-- speed-ups                                      
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	local loader = parameter.loader -- Number
	local target = parameter.target -- Table
  
  targetPosition = Vec3(position[1], position[2], position[3])
  
  Spring.Echo(targetPosition)
  
  x,y,z = SpringGetUnitPosition(loader)
  
  --check whether unit is around its position, if yes, return SUCCESS 
  if(IsClose(x, z, targetPosition["x"], targetPosition["z"])) then
    return SUCCESS
  end
  
  -- move to the target location
  local cmdID = CMD.MOVE
  
  --SpringGiveOrderToUnit(loader, cmdID, targetPosition:AsSpringVector(), {})
  --targetPosition:AsSpringVector()
  
  return RUNNING
end                  

function Reset(self)
	ClearState(self)
end
