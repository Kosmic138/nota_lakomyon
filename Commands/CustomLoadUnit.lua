function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load target unit with selected loader.",
		parameterDefs = {
			{ 
				name = "loader",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "target",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end        

-- speed-ups                                      
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetGroundHeight = Spring.GetGroundHeight
local SpringGetUnitTransporter = Spring.GetUnitTransporter

local closeBenchmark = 3
local verticalCloseBenchmark = 46

local function ClearState(self)
  
end

function Run(self, units, parameter)
	local loader = parameter.loader -- Number
	local target = parameter.target -- Number
  
	local pointX, pointY, pointZ = SpringGetUnitPosition(target)
  targetPosition = Vec3(pointX, pointY, pointZ)
  
  x,y,z = SpringGetUnitPosition(loader)
  
  --check whether unit is around its position, if yes, return SUCCESS 
  --if(IsClose(x, z, targetPosition["x"], targetPosition["z"])) then
  --  return SUCCESS
  --end
  
  -- load units
  local cmdID = CMD.LOAD_UNITS
  
  SpringGiveOrderToUnit(loader, cmdID, {targetPosition["x"], targetPosition["y"], targetPosition["z"], 80}, {})
  
  x2, y2, z2 = SpringGetUnitPosition(target)
  Spring.Echo(SpringGetUnitTransporter(target))
  
  if(SpringGetUnitTransporter(target) == loader) then
    return SUCCESS
  end
  
  return RUNNING
end                  

function Reset(self)
	ClearState(self)
end
