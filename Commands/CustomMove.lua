function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by array of unitID",
		parameterDefs = {
			{ 
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			}
		}
	}
end   

local closeBenchmark = 80

local function IsClose(x1, z1, x2, z2)
  local result = true
  if(x1 > x2) then
    if(x1 - x2 > closeBenchmark) then
      result = false
    end
  else
    if(x2 - x1 > closeBenchmark) then
      result = false
    end
  end
  
  if(z1 > z2) then
    if(z1 - z2 > closeBenchmark) then
      result = false
    end
  else
    if(z2 - z1 > closeBenchmark) then
      result = false
    end
  end
  return result
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	local unit = parameter.unit -- Number
	local position = parameter.position -- Vec3
	local fight = parameter.fight -- boolean   
  
  x,y,z = SpringGetUnitPosition(unit)
  
  --check whether unit is around its position, if yes, return SUCCESS 
  if(IsClose(x, z, position["x"], position["z"])) then
    return SUCCESS
  end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

  SpringGiveOrderToUnit(unit, cmdID, position:AsSpringVector(), {})
	
	return RUNNING
end


function Reset(self)
	ClearState(self)
end
