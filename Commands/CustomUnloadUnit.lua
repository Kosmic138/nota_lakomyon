function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by array of unitID",
		parameterDefs = {
			{ 
				name = "loader",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}, 
			{ 
				name = "cargo",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end 

local radius = 600 

local closeBenchmark = 5
local verticalCloseBenchmark = 40 

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitTransporter = Spring.GetUnitTransporter

local function ClearState(self)
  self.running = false
end

function Run(self, units, parameter)

	local unit = parameter.loader -- Number
  local cargo = parameter.cargo -- Number
	local position = parameter.position -- Vec3 
  
  if(unit == nil) then
    return SUCCESS
  end
  
  x,y,z = SpringGetUnitPosition(unit)
  x2,y2,z2 = SpringGetUnitPosition(cargo)
  
  --check whether unit is around its position, if yes, return SUCCESS
  
  --if(self.unloaded) then
  --  return SUCCESS
  --end
  
  if(SpringGetUnitTransporter(cargo) == nil) then
    self.running = false
    self.unloaded = true
    return SUCCESS
  end 
    
  if(self.running) then
    return RUNNING
  end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.UNLOAD_UNITS

  SpringGiveOrderToUnit(unit, cmdID, {position["x"], position["y"], position["z"], radius}, {})
  
  self.running = true
	
	return RUNNING
end


function Reset(self)
	ClearState(self)
end
